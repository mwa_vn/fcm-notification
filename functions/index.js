'use strict';
const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

exports.fcmSend = functions.database
  .ref('notifications/{userId}/{postId}')
  .onCreate(async (snapshot, context) => {
    const userId = context.params.userId;
    const notificationTokens = await admin.database().ref(`fcmTokens/${userId}/notificationTokens`).once('value');
    if (!notificationTokens.exists()) {
      return console.log('There are no notification tokens to send to.');
    }
    const userFcmTokens = Object.keys(notificationTokens.val());
    const { notification } = snapshot.val();
    const postId = context.params.postId;
    const payload = { notification };
    payload.data = { postId };
    const response = await admin.messaging().sendToDevice(userFcmTokens, payload);
    // For each message check if there was an error.
    const tokensToRemove = [];
    response.results.forEach((result, index) => {
      const error = result.error;
      if (error) {
        console.error('Failure sending notification to', userFcmTokens[index], error);
        // Cleanup the tokens who are not registered anymore.
        if (error.code === 'messaging/invalid-registration-token' ||
            error.code === 'messaging/registration-token-not-registered') {
          tokensToRemove.push(notificationTokens.ref.child(userFcmTokens[index]).remove());
        }
      }
    });
    return Promise.all(tokensToRemove);
  });

const nodemailer = require('nodemailer');
const gmailEmail = functions.config().gmail.email;
const gmailPassword = functions.config().gmail.password;
const mailTransport = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: gmailEmail,
    pass: gmailPassword,
  },
});
exports.sendEmail = functions.database
  .ref('emails/unhealthy-posts/{emailId}')
  .onCreate(async (snapshot, context) => {
    const {
      to,
      subject,
      content
    } = snapshot.val();
    const mailOptions = {
      from: '"MWA-VN social network." <noreply@mwa-vn.firebaseapp.com>',
      to,
      subject,
      text: content
    };
    try {
      await mailTransport.sendMail(mailOptions);
      console.log(`New ${context.params.emailId} email sent to:`, to);
    } catch(error) {
      console.error('There was an error while sending the email:', error);
    }
    return null;
  });